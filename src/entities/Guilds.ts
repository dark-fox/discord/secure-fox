import { BaseEntity, Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Index([ 'active' ])
@Index([ 'created' ])
@Unique([ 'discordId' ])
export class Guilds extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ name: 'discord_id' })
  discordId!: string;

  @Column()
  name!: string;

  @Column('boolean', { default: true })
  active!: boolean;

  @CreateDateColumn()
  created!: Date;

}