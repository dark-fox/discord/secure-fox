//Base
import app from '../../package.json';
import commands from '../../config/commands.json';
import commands_settings from '../../config/commands_settings.json';
import {
  CollectorFilter,
  EmbedFieldData,
  MessageEmbed,
  MessageReaction,
  ReactionCollector,
  User
} from 'discord.js';

//Tools
import { get, find } from 'lodash';
import { Command } from './Command';
import { Time } from '../helpers/Time';
import { ICommandType, IHelpView, ISingleCommand } from './interfaces/ICommand';
import { EmbedModel } from '../model/EmbedModel';

export class Info extends Command {
  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return this.processUnknownCommand();
      case 'info':
        return await this.processInfo();
      case 'help':
        return await this.processHelp();
      case 'help-admin':
        return await this.processHelpAdmin();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    return this;
  }

  /**
   * Change prefix.
   */
  protected async processInfo(): Promise<this> {
    const version: string = app.version;
    const uptime: number = process.uptime()
    const Embed: MessageEmbed = EmbedModel.infoEmbed('', '');

    Embed
      .setAuthor('BotCore Bot Information', this.Client.getBotAvatar())
      .addFields(
        { name: 'Version', value: version, inline: true },
        { name: 'Uptime', value: Time.convertSecondsToTime(uptime), inline: true },
      );

    await this.sendMessage(Embed);

    return this;
  }

  /**
   * Create interactive Embed with help.
   */
  protected async processHelp(adminCommands: boolean = false): Promise<this> {
    const Embed: MessageEmbed = EmbedModel.infoEmbed('Use reactions listed below to display help for each type.', 'Here you can see all possible commands.');
    const views: IHelpView = {
      help: [],
    };
    let reactionEmojis: string[] = ['ℹ'];

    Embed
      .setAuthor('Welcome to BotCore Bot Help', this.Client.getBotAvatar())
      .setFooter(`BotCore version ${app.version}`, this.Client.getBotAvatar())
      .addField('ℹ help', 'Display this screen.');

    for (const type of commands_settings.types) {
      if (adminCommands || type.visible) {
        views.help.push({name: `${type.emoji} ${type.name}`, value: type.description})
        reactionEmojis.push(type.emoji);
      }
    }

    reactionEmojis.push('❌');
    views.help.push({name: `❌ close`, value: 'close this menu'})

    for (const command of Object.keys(commands)) {
      const commandData: ISingleCommand = get(commands, command, {});
      const commandName: string = await this.isCommandDisabled(command) ? `~~${command}~~` : command;

      if (commandData && (adminCommands || commandData.visible)) {
        if(!views.hasOwnProperty(commandData.type)) {
          views[commandData.type] = [];
        }

        views[commandData.type].push({name: commandName, value: commandData.help});
      }
    }

    Embed.addFields(views.help);

    await this.sendMessage(Embed)
      .then(async (Message) => {
        let processMessage: boolean = true;
        const filter: CollectorFilter = (Reaction: MessageReaction, user: User) => {
          return reactionEmojis.includes(Reaction.emoji.name) && user.id === this.Message.author.id;
        };

        //React with emoji in correct order
        for (const emoji of reactionEmojis) {
          await Message.react(emoji);
        }

        const Collector: ReactionCollector = Message.createReactionCollector(filter, { time: 60000 * 5});

        //Process content change for all of possible reaction roles
        Collector.on('collect', (Reaction: MessageReaction) => {
          let fields: EmbedFieldData[] = [];
          let title: string = '';
          let description: string = '';

          if ('ℹ' === Reaction.emoji.name) {
            title = 'Here you can see all possible commands.';
            description = 'Use reactions listed below to display help for each type.';
            fields = views.help;
          } else if ('❌' === Reaction.emoji.name) {
            Message.delete();
            processMessage = false;
            return;
          } else {
            const command: ICommandType|undefined = find(commands_settings.types, {emoji: Reaction.emoji.name});
            description = '';

            if (command) {
              const type = command.name;

              if (views.hasOwnProperty(type)) {
                title = `Help for ${type}`;
                fields = views[type];
              }
            }
          }

          if (processMessage) {
            const EditedEmbed: MessageEmbed = EmbedModel
              .infoEmbed(description, title)
              .setAuthor('Welcome to BotCore Bot Help', this.Client.getBotAvatar())
              .setFooter(`BotCore version ${app.version}`, this.Client.getBotAvatar());

            Message.edit(EditedEmbed.addFields(fields));

            //Remove user reaction
            const userReactions = Message.reactions.cache.filter(reaction => reaction.users.cache.has(this.Message.author.id));

            try {
              for (const reaction of userReactions.values()) {
                reaction.users.remove(this.Message.author.id);
              }
            } catch (error) {
              console.error('[ERROR] Info.processHelp() > Collector.on(collect) > reaction.users.remove');
              console.error(error);
            }
          }
        });

        //Hide menu when time expires
        Collector.on('end', (Reaction: MessageReaction) => {
          if (processMessage) {
            const EditedEmbed: MessageEmbed = EmbedModel
              .warningEmbed('Help menu expired. Type `help` command again if you want to see help once again.', 'Session expired')
              .setAuthor('Welcome to BotCore Bot Help', this.Client.getBotAvatar())
              .setFooter(`BotCore version ${app.version}`, this.Client.getBotAvatar());

            Message.edit(EditedEmbed);
            Message.reactions.removeAll();
          }
        });
      });

    return this;
  }

  /**
   * Wrapper for displaying admin-only commands.
   */
  protected async processHelpAdmin(): Promise<this> {
    if (await this.hasPermission()) {
      return this.processHelp(true);
    }

    return await this.sendNoPermissionMessage();
  }

}