//Core
import { Channel } from 'discord.js';

//Base
import { Command } from './Command';

//Model
import { EmbedModel } from '../model/EmbedModel';

export class Settings extends Command {
  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return this.processUnknownCommand();
      case 'prefix':
        return await this.processPrefix();
      case 'add-permission':
        return await this.processAddPermission();
      case 'remove-permission':
        return await this.processRemovePermission();
      case 'permitted-roles':
        return await this.processPermittedRoles();
      case 'enable-command':
        return await this.processEnableCommand();
      case 'disable-command':
        return await this.processDisableCommand();
      case 'disabled-commands':
        return await this.processDisabledCommands();
      case 'reports-channel':
        return await this.processReportsChannel();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    this.GuildSettings.setGuildId(this.guildId);
    return this;
  }

  /**
   * Change prefix.
   */
  protected async processPrefix(): Promise<this> {
    if (this.isAdmin()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const newPrefix: string = this.arguments[0] || '';

        if (1 <= newPrefix.length) {
          await this.GuildSettings.setPrefix(newPrefix)
            .then(response => this.displayModelResponse(response))
            .catch(error => {
              this.sendMessage(EmbedModel.errorEmbed(`Some database issue happened and new prefix can't be stored.`));

              console.error(`[ERROR] Can't store prefix "${newPrefix}" in database for guild "${this.guildId}".`);
              console.error(error);
            });
        } else {
          await this.sendMessage(EmbedModel.errorEmbed('Prefix should be at least single character.'));
        }
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Add role to permitted list.
   */
  protected async processAddPermission(): Promise<this> {
    if (this.isAdmin()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const newRole: string = this.arguments.join(' ').trim();

        await this.GuildSettings.addRole(newRole)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] GuildSettings.processAddPermission() error');
            console.error(error);
          });
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Remove role from permitted list.
   */
  protected async processRemovePermission(): Promise<this> {
    if (this.isAdmin()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const roleToDelete: string = this.arguments.join(' ').trim();
        const permittedRoles: string[]|undefined|null = this.Settings?.permittedRoles;

        if (permittedRoles) {
          await this.GuildSettings.removeRole(roleToDelete)
            .then(response => this.displayModelResponse(response))
            .catch(error => {
              console.error('[ERROR] GuildSettings.processAddPermission() error');
              console.error(error);
            });
        }
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Show list of permitted roles.
   */
  protected async processPermittedRoles(): Promise<this> {
    if (await this.hasPermission()) {
      const permittedRoles: string[] = await this.getPermittedRoles();
      let result: string = '';

      for (let role of permittedRoles) {
        if (0 < result.length) {
          result += ', ';
        }

        result += `\`${role}\``;
      }

      result = result.trim();

      if (0 <= result.length && '' !== result) {
        await this.sendMessage(result);
      } else {
        await this.sendMessage('Role Settings are\'t defined.');
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Remove command from disable commands list.
   */
  protected async processEnableCommand(): Promise<this> {
    if (await this.hasPermission()) {
      const commandName: string = this.arguments[0] || '';

      await this.GuildSettings.enableCommand(commandName)
        .then(response => this.displayModelResponse(response))
        .catch(error => {
          console.error('[ERROR] GuildSettings.processEnableCommand() error');
          console.error(error);
        });
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Add command to disable commands list.
   */
  protected async processDisableCommand(): Promise<this> {
    if (await this.hasPermission()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const commandName: string = this.arguments[0] || '';

        if (this.isCommandProtected(commandName)) {
          await this.sendMessage(EmbedModel.errorEmbed('You can\'t disable this command because is marked as protected.'));
        } else {
          await this.GuildSettings.disableCommand(commandName)
            .then(response => this.displayModelResponse(response))
            .catch(error => {
              console.error('[ERROR] GuildSettings.processDisableCommand() error');
              console.error(error);
            });
        }
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Display list of disabled commands.
   */
  protected async processDisabledCommands(): Promise<this> {
    if (await this.hasPermission()) {
      const disabledCommands: string[] = this.Settings?.disabledCommands || [];

      if (0 < disabledCommands.length) {
        let result: string = '';

        for (let command of disabledCommands) {
          if (0 < result.length) {
            result += ', ';
          }

          result += `\`${command}\``;
        }

        result = result.trim();

        await this.sendMessage(`Disabled commands:\n${result}`);
      } else {
        await this.sendMessage('All commands are enabled.');
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Set specific channels to sending reports.
   */
  protected async processReportsChannel(): Promise<this> {
    if (await this.hasPermission()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const turnOff: boolean = !!(this.arguments[0] && 'off' === this.arguments[0].toLowerCase());
        let channel: string|null;

        if (turnOff) {
          channel = null;
        } else {
          let channels: string[] = [];

          this.Message.mentions.channels.each((Channel: Channel) => {
            channels.push(Channel.id);
          });

          if (0 >= channels.length) {
            await this.sendMessage('You have to mention channel if you want to save it.');
            return this;
          } else {
            channel = channels[0];
          }
        }

        await this.GuildSettings.setReportChannel(channel)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] GuildSettings.setReportChannel() error');
            console.error(error);
          });
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

}
