//Base
import { EmbedFieldData, Message } from 'discord.js';

//Core
import { BotClient } from '../../core/BotClient';

export interface ISingleCommand {
  type: string;
  help: string;
  visible: boolean;
}

export interface ICommandType {
  name: string;
  description: string;
  visible: boolean;
  emoji: string;
}

export interface IHelpView {
  [key: string]: EmbedFieldData[];
}

export interface ICommand {
  process(): Promise<this>;
  setSettings(): Promise<this>;
  setGuildId(id: number): this;
  setCommand(command: string[]): this;
  setClient(Client: BotClient): this;
  setMessage(Message: Message): this;
  isCommandDisabled(command?: string): Promise<boolean>;
}