//Base
import config from '../../config/config.json';

//Entities
import { Settings } from '../entities/Settings';

export class Prefix {
  protected GuildSettings!: Settings|undefined;

  private prefixes: string[] = [];

  /**
   * Set all data necessary for processing Prefix.
   *
   * @param {Settings|undefined} GuildSettings - instance of Guild Settings.
   */
  public async setData(GuildSettings: Settings|undefined): Promise<this> {
    this.GuildSettings = GuildSettings;
    return await this.setPrefixes();
  }

  /**
   * Check if prefix given by user is correct.
   *
   * @param {string} prefix - Prefix given by user.
   */
  public isPrefixCorrect(prefix: string): boolean {
    return 0 <= this.prefixes.indexOf(prefix);
  }

  /**
   * Return an array of prefixes.
   */
  public getPrefixes(): string[] {
    return this.prefixes;
  }

  /**
   * Clean prefixes array.
   */
  public cleanPrefixes(): this {
    this.prefixes = [];
    return this;
  }

  /**
   * Fill prefixes array with content. We need array because due to Settings,
   */
  private async setPrefixes(): Promise<this> {
    let prefix: string = '';

    this.cleanPrefixes();

    if (this.GuildSettings) {
      prefix = this.GuildSettings.prefix;

      if (0 >= prefix.length) {
        this.setDefaultPrefix();
      } else {
        if (!this.GuildSettings.prefixCaseSensitive) {
          prefix = prefix.toLowerCase();
          const prefixUpperCase = prefix.toUpperCase();

          if (prefixUpperCase !== prefix) {
            this.prefixes.push(prefix);
            this.prefixes.push(prefixUpperCase);
          } else {
            this.prefixes.push(prefix);
          }
        } else {
          this.prefixes.push(prefix);
        }
      }
    } else {
      this.setDefaultPrefix();
    }

    return this;
  }

  /**
   * Set possible prefixes based on data stored in config.json file.
   */
  private setDefaultPrefix() {
    let prefix = config.prefix;

    if (config.settings.prefix.caseSensitive) {
      prefix = prefix.toLowerCase();
      const prefixUpperCase = prefix.toUpperCase();

      if (prefixUpperCase !== prefix) {
        this.prefixes.push(prefix);
        this.prefixes.push(prefixUpperCase);
      } else {
        this.prefixes.push(prefix);
      }
    } else {
      this.prefixes.push(prefix);
    }
  }

}