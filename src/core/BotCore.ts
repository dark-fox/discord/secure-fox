//Base
import { Message } from 'discord.js';

//Core
import { MessageProcessor } from './MessageProcessor';
import { BotClient } from './BotClient';
import { QueueManager } from './QueueManager';

export class BotCore {
  public MessageProcessor: MessageProcessor;
  protected Queue: QueueManager;
  private ready: boolean = false;

  public constructor() {
    this.MessageProcessor = new MessageProcessor;
    this.Queue = new QueueManager;
  }

  /**
   * Run the entire bot.
   */
  public async run(): Promise<this> {
    this.Queue.run();
    this.ready = true;

    return this;
  }

  /**
   * Set Discord.js Client instance.
   *
   * @param {BotClient} Client - Discord.js Client instance.
   */
  public async setClient(Client: BotClient): Promise<this> {
    this.MessageProcessor.setClient(Client);
    this.Queue.setClient(Client);
    return this;
  }

  /**
   * Return true when bot is ready to work.
   */
  public async isReady(): Promise<boolean> {
    return this.ready;
  }

  /**
   * Start processing Message.
   *
   * @param {Message} message - Discord.js Message instance.
   */
  public async processMessage(message: Message): Promise<MessageProcessor> {
    return await this.MessageProcessor.setMessage(message).process();
  }

}