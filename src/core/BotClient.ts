import { Client } from 'discord.js';

export class BotClient {
  private readonly Client: Client;

  constructor(Client: Client) {
    this.Client = Client;
  }

  /**
   * Return url to bot Avatar if it's possible.
   */
  public getBotAvatar(): string {
    return this.Client.user?.avatarURL() || '';
  }

  /**
   * Return current instance of Discord.js Client.
   */
  public getClient(): Client {
    return this.Client;
  }

}