export class CommandProcessor {
  /**
   * Combine users commands into one string.
   *
   * @param {string[]} command   Array with commands written by user.
   * @param {string}   separator Glue to put between array elements.
   * @param {number}   startAt   How many commands should we delete from the beginning of base array?
   */
  public static mergeCommands(command: string[], separator: string = ' ', startAt: number = 0): string {
    const cutCommand: string[] = command.slice(startAt);
    return cutCommand.join(separator);
  }

  /**
   * Cut given string into an array.
   *
   * @param {string} commandString String that needs to be cut.
   * @param {string} separator     Separator what will be used co create an array based on given string.
   */
  public static cutString(commandString: string, separator: string = '|'): string[] {
    return commandString.split(/\|/);
  }

}