export class Validation {
  /**
   * Check if given value is correct HEX.
   *
   * @param {string} test
   */
  public static isHex(test: string): boolean {
    return /^#[0-9A-Fa-f]{6}$/i.test(test);
  }

  public static isImageUrl(test: string): boolean {
    return /(http(s?):)([/|.\w\s-])*\.(?:jpg|gif|png|jpeg|bmp|webp)/g.test(test);
  }

}