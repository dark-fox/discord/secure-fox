//Base
import * as cron from 'node-cron';

//Core
import { Reporter } from '../core/Reporter';
import { BotClient } from '../core/BotClient';

//Queue
import { IQueueTask } from './interfaces/IQueueTask';

export abstract class QueueTask implements IQueueTask {
  protected expression: string = '* */1 * * *';
  protected Client!: BotClient;
  protected Reporter: Reporter;

  protected constructor() {
    this.Reporter = new Reporter;
  }

  /**
   * Run Cron Job.
   */
  public run(Client: BotClient): IQueueTask {
    this.setClient(Client);

    cron.schedule(this.expression, async () => {
      await this.callback();
    });

    return this;
  }

  /**
   * Save Client to class property.
   *
   * @param {BotClient} Client - Instance of BotClient.
   */
  public setClient(Client: BotClient): QueueTask {
    this.Client = Client;
    return this;
  }

  /**
   * Callback to run by Cron Job.
   */
  public abstract callback(): Promise<QueueTask>;

}